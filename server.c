/*Сборка g++ -static -Wall -o "%e" "%f" -lprotobuf -lpthread */
#include </home/asmodeus/Documents/Study/KR/every.h>

void readyReceive(struct sockaddr_in addrCl, struct sockaddr_in addr);
void readySend(struct sockaddr_in addrCl, struct sockaddr_in addr);
void *receiveMessageServer(void *num);
void *sendMessageServer(void *num);
void end(int param);

int query = 0;
int listener, listener2;
pthread_mutex_t mutex;
struct message *my_msg;

int main()
{
	signal(SIGINT, end);
	my_msg = (struct message *)malloc(sizeof(struct message)*QUERY_SIZE);
	pthread_t sender, receiver;
	
	struct sockaddr_in addr, addr2, addrS, addrR;

	if((listener = socket(AF_INET, SOCK_STREAM, 0)) < 0)
		die((char *)"socket");
		
	if((listener2 = socket(AF_INET, SOCK_STREAM, 0)) < 0)
		die((char *)"socket");
    
	addr = createAddr(ADDR_CL_S);
	if(bind(listener, (struct sockaddr *)&addr, sizeof(addr)) < 0)
		die((char *)"bind");
		
	addr2 = createAddr(ADDR_CL_R);
	if(bind(listener2, (struct sockaddr *)&addr2, sizeof(addr)) < 0)
		die((char *)"bind");
		
	addrS = createAddrUDP(ADDR_S);
	addrR = createAddrUDP(ADDR_R);

	listen(listener, 10);
	listen(listener2, 10);
    
	pthread_create(&receiver, NULL, receiveMessageServer, &listener);
	pthread_create(&sender, NULL, sendMessageServer, &listener2);
	
	while(1) 
	{
		readyReceive(addrS, addr);
		readySend(addrR, addr2);
		sleep(1);
	}
    
	return 0;
}

/* Получение сообщения сервером */
void *receiveMessageServer(void *num)
{
	int listener=*((int *) num);
	Msg mes;
	struct message *rec_msg;
	int sock;
	while(1)
	{
		if(query < QUERY_SIZE)
		{
			if((sock = accept(listener, NULL, NULL)) < 0)
				die((char *)"accept");
			pthread_mutex_lock(&mutex);
			recv(sock, &mes, sizeof(mes), 0);
			rec_msg = my_msg + query;
			unpack(&mes, rec_msg);
			query++;
			pthread_mutex_unlock(&mutex);
			printf("Сообщение получено, сообщений в очереди %d\n", query);
			close(sock);
		}
	}
	return 0;
}

/* Отправка сообщения сервером */
void *sendMessageServer(void *num)
{
	int listener=*((int *) num);
	Msg mes;
	int sock, i;
	while(1)
	{
		if(query > 0)
		{
			if((sock = accept(listener, NULL, NULL)) < 0)
				die((char *)"accept");
			pthread_mutex_lock(&mutex);
			pack(&mes, my_msg);
			send(sock, &mes, sizeof(mes), 0);
			for(i = 1; i <= query; i++)
				*(my_msg + i - 1) = *(my_msg + i);
			query--;
			pthread_mutex_unlock(&mutex);
			printf("Сообщение отправлено, сообщений в очереди %d\n", query);
			close(sock);
		}
	}
	return 0;
}

/* Рассылка UDP-собщений отправителям */
void readyReceive(struct sockaddr_in addrCl, struct sockaddr_in addr)
{
	struct act action;
	action.action = SEND;
	action.addr = addr;
	int broadcastEnable = 1;
	int sockUDP;
	if((sockUDP = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
		die((char *)"socket");	
	if(setsockopt(sockUDP, SOL_SOCKET, SO_BROADCAST, (char *)&broadcastEnable, sizeof(broadcastEnable)) < 0)
		die((char *)"setsockopt");
	if(query < QUERY_SIZE)
	{
		sendto(sockUDP, &action, sizeof(struct act), 0, (struct sockaddr *)&addrCl, sizeof(addrCl));
	}
	close(sockUDP);
	sleep(1);
}

/* Рассылка UDP-собщений получателям */
void readySend(struct sockaddr_in addrCl, struct sockaddr_in addr)
{
	struct act action;
	action.action = REQUEST;
	action.addr = addr;
	int broadcastEnable = 1;
	int sockUDP;
	if((sockUDP = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
		die((char *)"socket");	
	if(setsockopt(sockUDP, SOL_SOCKET, SO_BROADCAST, (char *)&broadcastEnable, sizeof(broadcastEnable)) < 0)
		die((char *)"setsockopt");
	if(query > 0)
	{
		sendto(sockUDP, &action, sizeof(struct act), 0, (struct sockaddr *)&addrCl, sizeof(addrCl));
	}
	close(sockUDP);
	sleep(1);
}

/* Завершающее сообщение, чтобы при отключении сервера закрыть сокеты и освободить память */
void end(int param)
{
	printf("\nЗавершение работы сервера\n");
	close(listener);
	close(listener2);
	free(my_msg);
	exit(0);
}
