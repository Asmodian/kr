/*Сборка g++ -static -Wall -o "%e" "%f" -lprotobuf -lpthread */
#include </home/asmodeus/Documents/Study/KR/every.h>

void end(int param);

int sockUDP;

int main()
{
	signal(SIGINT, end);
	int broadcastEnable = 1;
	
	struct sockaddr_in addrUDP, servAddr;
	unsigned int servAddrLen;
	servAddrLen = sizeof(servAddr);
	struct message my_msg;
	struct act action;
    
    if((sockUDP = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
		die((char *)"socket");
	if(setsockopt(sockUDP, SOL_SOCKET, SO_BROADCAST, (char *)&broadcastEnable, sizeof(broadcastEnable)) < 0)
		die((char *)"setsockopt");

	addrUDP = createAddrUDP(ADDR_R);
	if(bind(sockUDP, (struct sockaddr *)&addrUDP, sizeof(addrUDP)) < 0)
		die((char *)"bind");
	
	while(1)
	{
		recvfrom(sockUDP, &action, sizeof(struct act), 0, (struct sockaddr *)&servAddr, &servAddrLen);
		if(action.action == REQUEST)
		{
			action.addr.sin_addr.s_addr = servAddr.sin_addr.s_addr;
			my_msg = receiveMessage(&action.addr);
			sleep(my_msg.timeToWork);
		}
	}
    close(sockUDP);
	return 0;
}

void end(int param)
{
	printf("\nЗавершение работы получателя\n");
	close(sockUDP);
	exit(0);
}
