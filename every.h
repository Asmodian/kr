#include <stdlib.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <signal.h>
#include </home/asmodeus/Documents/Study/KR/msg.pb.h>
#include </home/asmodeus/Documents/Study/KR/msg.pb.cc>
using namespace Msg_description;

#define MAX_SIZE 1024
#define QUERY_SIZE 10


/* Используемые порты */
#define ADDR_S 3545
#define ADDR_R 3546
#define ADDR_CL_S 3541
#define ADDR_CL_R 3542

#define REQUEST 1
#define SEND 2

/* Структура сообщения */
struct message{
	int timeToWork;
	int msgLen;
	char msg[1024];
};

/* Сообщение UDP */
struct act{
	int action;
	struct sockaddr_in addr;
};

/* Завершение работы по ошибке */
void die(char *s) //for all
{
	perror(s);
	exit(1);
}

/* Создание структуры с адресом для TCP */
struct sockaddr_in createAddr(int port)
{
	struct sockaddr_in addr;
	//char address[] = "192.168.0.0/16";
	bzero(&addr, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
	//inet_aton(address, &addr.sin_addr);
	return addr;
}

/* Создание структуры с адресом для UDP */
struct sockaddr_in createAddrUDP(int port)
{
	struct sockaddr_in addr;
	//char address[] = "192.168.0.0/16";
	bzero(&addr, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = htonl(INADDR_BROADCAST);
	//inet_aton(address, &addr.sin_addr);
	return addr;
}

/* Запаковать в protobuf */
void pack(Msg_description::Msg *mes, struct message *my_msg)
{
	mes->set_timetowork(my_msg->timeToWork);
	mes->set_msglen(my_msg->msgLen);
	mes->set_msgbuf(my_msg->msg);
}

/* Распаковать из protobuf */
/* Ошибка при распаковке строки */
void unpack(Msg_description::Msg *mes, struct message *my_msg)
{
	my_msg->timeToWork = mes->timetowork();
	my_msg->msgLen = mes->msglen();
	//strncpy(my_msg->msg, mes->msgbuf().c_str(), my_msg->msgLen);
}

/* Создание сообщения */
struct message createMessage() //for sender
{
	struct message my_msg;
	srand(getpid());
	my_msg.timeToWork = rand()%10;
	sprintf(my_msg.msg, "%d", getpid());
	my_msg.msgLen = strlen(my_msg.msg);
	return my_msg;
}

/* Отправка сообщения */
void sendMessage(struct sockaddr_in* addr, struct message* my_msg) //for sender
{
	int sock;
	Msg mes;
	pack(&mes, my_msg);
	if((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
		die((char *)"socket");
	if(connect(sock, (struct sockaddr *)addr, sizeof(*addr)) < 0)
		die((char *)"connect");
	
	send(sock, &mes, sizeof(mes), 0);
	printf("Message was send\n");
	close(sock);
}

/* Получение сообщения в protobuf */
struct message receiveMessage(struct sockaddr_in* addr) //for receiver
{
	int sock;
	struct message my_msg;
	Msg *mes;
	mes = (Msg *)malloc(sizeof(Msg));
	if((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
		die((char *)"socket");
	if(connect(sock, (struct sockaddr *)addr, sizeof(*addr)) < 0)
		die((char *)"connect");
	recv(sock, mes, sizeof(*mes), 0);
	unpack(mes, &my_msg);
	printf("Message was recieved\n");
	close(sock);
	free(mes);
	return my_msg;
}
